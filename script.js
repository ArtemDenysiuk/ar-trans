function getPrice() {
	var distance = parseFloat(document.getElementById('distance').value);
	var weight = parseFloat(document.getElementById('weight').value);
	var fragilitycoeficient = document.getElementById("fragilitylist").value;
	var announcedPrice = parseFloat(document.getElementById('announcedPrice').value);
	
	var result = fragilitycoeficient * (0.2 * Math.floor(roundPlus(distance,3)) 
	* 0.3 * Math.floor(roundPlus(weight,3)) + 0.2 * announcedPrice / 2) / 2 + 0.2 * announcedPrice;
	document.getElementById('result').innerHTML = 'Ціна перевезення для вас - ' + roundPlus(result, 2);
	
};

function roundPlus(x, n) {
	if(isNaN(x) || isNaN(n)) 
		return false;
	var m = Math.pow(10,n);
	return Math.round(x*m)/m;
}

function clock(){
	var date = new Date(),
	hours = (date.getHours() < 10) ? '0' + date.getHours() : date.getHours(),
	minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes(),
	seconds = (date.getSeconds() < 10) ? '0' + date.getSeconds() : date.getSeconds();
	document.getElementById('clock').innerHTML = hours + ':' + minutes + ':' + seconds;
}
setInterval(clock, 1000);
clock();	

buttonToTop = document.getElementById("buttonToTop");
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    buttonToTop.style.display = "block";
  } else {
    buttonToTop.style.display = "none";
  }
}

function topFunction() {
  $('html, body').animate({ scrollTop: 0 }, 'slow');
} 